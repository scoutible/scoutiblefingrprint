import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/main.scss';

const authToken = window.localStorage.getItem('app/Auth/TOKEN')
  || window.sessionStorage.getItem('app/Auth/TOKEN');
const reportID = window.location.pathname.replace(/.*\//,'');

ReactDOM.render(
  <App
    authToken={authToken || ""}
    reportID={reportID || ""}
  />,
  document.getElementById('root')
);
