import React from 'react';
import ProgressBar from 'react-bootstrap/lib/ProgressBar'

class FingerprintDomainFacet extends React.Component {

  CreatePowerBarSegments() {
    const units = this.props.segments;
    let segments = [];
    for (let i = 0; i < units; i++) {
      segments.push(
        <div
          key={i}
          className="power-bar-segment"
          style={{ width: `${100 / units}%` }}
        />
      );
    }
    return segments;
  }

  value() {
    const unit = 100 / this.props.segments;
    let value = this.props.value;
    value = Math.max(value, 0);
    value = Math.min(value, 100);
    if (!!this.props.quantize) {
      value = Math.round(value / unit);
      if (value < 1) {
        value = 1;
      }
      value *= unit;
    }
    return value;
  }

  render = () => (
    <div className="domain-section domain-facet row">
      <div className='col-sm-4 domain-facet-label text-left'>
        <h3>{this.props.name}</h3>
      </div>
      <div className="col-sm-8 progress-facet-container">
        <div className="power-bar-segment-holder">
          {this.CreatePowerBarSegments()}
        </div>
        <ProgressBar className="progress-facet" now={this.value()} />
      </div>
    </div>
  )
}

export default FingerprintDomainFacet;
