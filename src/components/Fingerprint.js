import React from 'react';
import FingerprintSummary from './FingerprintSummary';
import FingerprintTeam from './FingerprintTeam';
import FingerprintDomain from './FingerprintDomain';
import FingerprintFooter from './FingerprintFooter';
import ScoutibleNavBar from './ScoutibleNavBar';


class Fingerprint extends React.Component {

  render() {
    const { user, fp } = this.props;
    const { applicant, org, job, scores, team, statements, code, group } = fp;
    const domains = [
      {
        id: 'bandwidth',
        name: 'Bandwidth',
        icon: 'https://storage.googleapis.com/scoutible/www/images/domain-icons/Bandwidth_Icon.png',
        text: 'Ability to think critically and solve problems',
        value: scores.bandwidth,
        facets: [
          {
            name: "Intellectual Capacity",
            value: scores.intellect,
          },
        ],
        spectrums: [
          {
            name: "Focus",
            leftLabel: "Near Term",
            rightLabel: "Long Term",
            value: scores.focus,
          },
        ],
        statements: statements.filter((item) => {
          const keys = item.keys;
          return (
            (keys.n === '+' && keys.o === '+') ||
            (keys.n === '+' && keys.o === '-') ||
            (keys.n === '-' && keys.o === '+') ||
            (keys.n === '-' && keys.o === '-') ||
            (keys.n === '+' && keys.c === '-') ||
            (keys.e === '+' && keys.o === '-') ||
            (keys.o === '+' && keys.a === '+') ||
            (keys.o === '+' && keys.a === '-') ||
            (keys.o === '+' && keys.c === '+') ||
            (keys.o === '+' && keys.c === '-') ||
            (keys.o === '-' && keys.c === '-') ||
            (keys.a === '-' && keys.c === '+') ||
            false
          );
        }),
      },
      {
        id: 'hustle',
        name: "Hustle",
        icon: "https://storage.googleapis.com/scoutible/www/images/domain-icons/Hustle_Icon.png",
        text: 'Determination and the drive to achieve',
        value: scores.hustle,
        facets: [
          {
            name: "Hardworking",
            value: scores.hardwork,
          },
        ],
        spectrums: [
          {
            name: "Autonomy",
            leftLabel: "Prefers Guidance",
            rightLabel: "Comfortable with Ambiguity",
            value: scores.autonomy,
          },
        ],
        statements: statements.filter((item) => {
          const keys = item.keys;
          return (
            (keys.n === '+' && keys.e === '-') ||
            (keys.n === '-' && keys.e === '+') ||
            (keys.n === '+' && keys.a === '+') ||
            (keys.n === '+' && keys.c === '+') ||
            (keys.n === '-' && keys.c === '+') ||
            (keys.n === '-' && keys.c === '-') ||
            (keys.e === '-' && keys.a === '+') ||
            (keys.e === '+' && keys.c === '+') ||
            (keys.e === '-' && keys.c === '+') ||
            (keys.e === '-' && keys.c === '-') ||
            (keys.o === '-' && keys.a === '+') ||
            (keys.o === '-' && keys.c === '+') ||
            (keys.a === '+' && keys.c === '+') ||
            (keys.a === '-' && keys.c === '-') ||
            false
          );
        }),
      },
      {
        id: 'social',
        name: 'Social',
        icon: 'https://storage.googleapis.com/scoutible/www/images/domain-icons/Social_Icon.png',
        text: 'Relationship navigation and teamwork',
        value: scores.social,
        facets: [
          {
            name: "Interpersonal Skills",
            value: scores.interpersonal,
          },
        ],
        spectrums: [
          {
            name: "Leadership",
            leftLabel: "Supporter",
            rightLabel: "Leader",
            value: scores.leadership,
          },
        ],
        statements: statements.filter((item) => {
          const keys = item.keys;
          return (
            (keys.n === '+' && keys.e === '+') ||
            (keys.n === '-' && keys.e === '-') ||
            (keys.n === '+' && keys.a === '-') ||
            (keys.n === '-' && keys.a === '+') ||
            (keys.n === '-' && keys.a === '-') ||
            (keys.e === '+' && keys.o === '+') ||
            (keys.e === '-' && keys.o === '+') ||
            (keys.e === '-' && keys.o === '-') ||
            (keys.e === '+' && keys.a === '+') ||
            (keys.e === '+' && keys.a === '-') ||
            (keys.e === '-' && keys.a === '-') ||
            (keys.e === '+' && keys.c === '-') ||
            (keys.o === '-' && keys.a === '-') ||
            (keys.a === '+' && keys.c === '-') ||
            false
          );
        }),
      },
    ];
    return (
      <div className="body">
        <ScoutibleNavBar
          user={user}
          breadcrumbs={[
            { text: org.name, href: "/jobs"},
            { text: job.name, href: `/jobs/${group}`},
            { text: applicant.name, href: "#"},
          ]}
        />
        <div className="fingerprint-container container">
          <FingerprintSummary
            name={applicant.name}
            company={org.name}
            job={job.name}
            email={applicant.email}
            phone={applicant.phone}
            bandwidth={scores.bandwidth}
            hustle={scores.hustle}
            social={scores.social}
            domains={domains}
          />
          <FingerprintTeam
            icon={team.icon}
            description={team.text}
          />
          {domains.map((domain) => (
            <FingerprintDomain
              key={domain.id}
              className={`fingerprint-domain ${domain.id}-domain`}
              {...domain}
            />
          ))}
        </div>
        <FingerprintFooter code={code} />
      </div>
    );
  }
}

export default(Fingerprint)
