import React from 'react';
import SpectrumBar from './SpectrumBar';

class FingerprintDomainSpectrum extends React.Component {

  RightHighlight() {
    if (this.props.value > 50) {
      return ([
        <div key='1' className="arrow-right domain-border-right-color" />,
        <div key='2' className="spectrum-highlight-right" />,
      ]);
    } else {
      return ( <div className="arrow-right" /> );
    }
  }

  LeftHighlight() {
    if (this.props.value < 50) {
      return ([
        <div key='1' className="arrow-left domain-border-left-color" />,
        <div key='2' className="spectrum-highlight-left" />,
      ]);
    } else {
      return ( <div className="arrow-left" /> );
    }
  }

  render = () => (
    <div className="domain-spectrum">
      <div className="domain-section row">
        <div className="col-sm-12">
          <h3 className="text-left">{this.props.name}</h3>
        </div>
      </div>
      <div className="domain-section-spectrum row">
        <div className="col-sm-6 spectrum-label">
          { this.LeftHighlight() }
          <h4 className="text-left">{this.props.leftLabel}</h4>
        </div>
        <div className="col-sm-6 spectrum-label">
          { this.RightHighlight() }
          <h4 className="text-right">{this.props.rightLabel}</h4>
        </div>
        <div className="col-sm-12 spectrum-bar-container">
          <SpectrumBar value={this.props.value} range={10} />
        </div>
      </div>
    </div>
  )
}

export default FingerprintDomainSpectrum;
