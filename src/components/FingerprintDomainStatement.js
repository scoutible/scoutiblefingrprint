import React from 'react';

class FingerprintDomainStatement extends React.Component {
  render = () => (
    <div className="col-sm-6 domain-statement-col">
      <div className="domain-statement" >  
        <div className="domain-statement-tag domain-color"><p>{this.props.name}</p></div>
        <div className="domain-statement-text"><p>{this.props.text}</p></div>
      </div>  
    </div>
  )
}

export default(FingerprintDomainStatement)
