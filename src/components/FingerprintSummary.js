import React from 'react';
import FingerprintSummaryDomain from './FingerprintSummaryDomain';


class FingerprintSummary extends React.Component {
  render = () => (
    <div className="fingerprint-summary row">
        <div className="contact col-sm-7">
        <h2>{this.props.name}</h2>
            <ul>
                <li><p>Applied</p><p>{ this.props.job }</p></li>
                <li><p>Company</p><p>{ this.props.company }</p></li> 
                <li><p>Email</p><p>{ this.props.email }</p></li>
                <li><p>Phone</p><p>{ this.props.phone }</p></li>
            </ul>    
          </div>
        <div className="summary-domains col-sm-5">
        {this.props.domains.map((elem, index) => (
          <FingerprintSummaryDomain key={index} className={elem.name.toLowerCase()+"-summary"} {...elem} />
        ))}
        </div>      
    </div>
  )
}

export default(FingerprintSummary)
