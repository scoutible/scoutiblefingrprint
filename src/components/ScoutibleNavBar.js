import React from 'react';
import Navbar from 'react-bootstrap/lib/Navbar'
import Nav from 'react-bootstrap/lib/Nav'
import NavDropdown from 'react-bootstrap/lib/NavDropdown'
import MenuItem from 'react-bootstrap/lib/MenuItem'
import Breadcrumb from 'react-bootstrap/lib/Breadcrumb'

// TODO: Figure out with Zach how to get the link back to their company page

class ScoutibleNavBar extends React.Component {
  render() {
    const { user } = this.props;
    const breadcrumbs = this.props.breadcrumbs || [];
    return (
      <Navbar fixedTop>
        <Nav pullLeft>
          <Breadcrumb>
            {breadcrumbs.map((item, index, array) => (
              <Breadcrumb.Item
                key={index}
                href={item.href}
                active={index === array.length - 1}
                >
                {item.text}
              </Breadcrumb.Item>
            ))}
          </Breadcrumb>
        </Nav>
        <div className="brand">
          <img alt="Scoutible" src="https://storage.googleapis.com/scoutible/www/images/Scoutible-Logotype-NavBar.png" />
        </div>
        <Nav pullRight>
          <NavDropdown eventKey={1} title={user.name} id="basic-nav-dropdown">
            <MenuItem eventKey={1.1} href="/jobs">
              Jobs
            </MenuItem>
            <MenuItem eventKey={1.1} href="/user/settings">
              Settings
            </MenuItem>
            <MenuItem eventKey={1.2} href="/signout">
              Sign Out
            </MenuItem>
          </NavDropdown>
        </Nav>
      </Navbar>
    );
  }
}

export default ScoutibleNavBar;
