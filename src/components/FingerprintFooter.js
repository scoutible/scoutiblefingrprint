import React from 'react';
import Navbar from 'react-bootstrap/lib/Navbar'
import Nav from 'react-bootstrap/lib/Nav'
import NavItem from 'react-bootstrap/lib/NavItem'

class FingerprintFooter extends React.Component {
  render = () => (
    <Navbar inverse className="fingerprint-footer">
      <Nav className="footer-code" pullRight={true} >
        <p className="code-label">Access Token Used</p>
        <div className="code-box">{this.props.code}</div>
      </Nav>
      <Nav className="footer-nav" pullLeft={true} >
        <img className="footer-brand" alt="Scoutible" src="https://storage.googleapis.com/scoutible/www/images/Scoutible-Logotype-White.png" />
        <p className="copyright">Copyright Scoutible, INC</p>
        <NavItem eventKey={1} href="#">Support</NavItem>
        <NavItem eventKey={2} href="#">Homepage</NavItem>
      </Nav>
    </Navbar>    
  )
}

export default(FingerprintFooter)
