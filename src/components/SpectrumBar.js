import React from 'react';

class SpectrumBar extends React.Component {

  values() {
    const width = this.props.range;
    const margin = width / 2;
    let mean = this.props.value;
    mean = Math.max(mean, margin);
    mean = Math.min(mean, 100 - margin);
    return {
      mean,
      min: (mean - margin),
      max: (mean + margin),
      width,
    };
  }

  styles() {
    const values = this.values();
    return { width: `${values.width}%`, marginLeft: `${values.min}%` };
  }

  render = () => (
    <div className="spectrum-bar">
      <div className="spectrum-centerline" />
      <div style={this.styles()} className="spectrum-bar-fill domain-color">
        <div className="spectrum-centermark" />
      </div>
    </div>
  )
}

export default SpectrumBar;
