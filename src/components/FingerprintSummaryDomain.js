import React from 'react';
import ProgressBar from 'react-bootstrap/lib/ProgressBar';

class FingerprintSummaryDomain extends React.Component {
  render = () => (
    <div className={`fingerprint-summary-domain ${this.props.className}`} >
      <p className="summary-domain-name">{this.props.name}</p>
      <p className="summary-domain-value">{this.props.value}</p>
      <img className="summary-domain-icon" alt={this.props.name} src={this.props.icon} />
      <ProgressBar now={this.props.value}>
      </ProgressBar>  
    </div>
  )
}

export default(FingerprintSummaryDomain)
