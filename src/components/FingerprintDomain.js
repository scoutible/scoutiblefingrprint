import React from 'react';
import FingerprintDomainSpectrum from './FingerprintDomainSpectrum'
import FingerprintDomainFacet from './FingerprintDomainFacet'
import FingerprintDomainStatement from './FingerprintDomainStatement'

class FingerprintDomain extends React.Component {

  render() {
    const { name, icon, text, value, facets, spectrums } = this.props;
    let statements = this.props.statements.sort((a, b) => b.value - a.value);
    statements = statements.slice(0, 2);
    return (
      <div className={this.props.className}>
        <div className="domain-section domain-header row">
          <div className="col-sm-1 domain-icon-holder">
            <img className="domain-color" alt="" src={icon} />
          </div>
          <div className="col-sm-9 domain-label">
            <h2 className="text-left">{name}</h2>
            <p className="text-left">{text}</p>
          </div>
          <div className="col-sm-2 domain-value">
            <p className="numerator text-right">{value}</p>
            <p className="denominator text-right">/100</p>
          </div>
        </div>
        {facets.map((elem, index) => (
          <FingerprintDomainFacet key={index} quantize segments={5} {...elem} />
        ))}
        {spectrums.map((elem, index) => (
          <FingerprintDomainSpectrum key={index} {...elem} />
        ))}
        <div className="domain-statements row">
          {statements.map((elem, index) => (
            <FingerprintDomainStatement key={index} {...elem} />
          ))}
        </div>
      </div>
    );
  }
}

export default FingerprintDomain;
