import React from 'react';

class FingerprintTeam extends React.Component {
  render = () => (
    <div className="fingerprint-team row">
      <div className="col-sm-1 col-sm-offset-1 team-icon-holder">
        <img alt="" src={this.props.icon} />
      </div>
      <div className="team-description-holder col-sm-9">
        <p className="team-description">{this.props.description}</p>
      </div>
    </div>  
  )
}

export default(FingerprintTeam)
