const data = [];

export default data;

data.push({
  version: 0,
  applicant: {
    name: "",
    email: "",
    phone: "",
  },
  org: {
    name: "",
  },
  job: {
    name: "",
  },
  code: "",
  scores: {
    // Bandwidth
    bandwidth: 50,
    intellect: 50,
    focus: 50,
    // Hustle
    hustle: 50,
    hardwork: 50,
    autonomy: 50,
    // Social
    social: 50,
    interpersonal: 50,
    leadership: 50,
  },
  team: {
    id: 0,
    name: "",
    icon: "",
    text: "",
  },
  statements: [
    {
      keys: { n: '0', e: '0' },
      value: 1,
      name: "NE",
      icon: "",
      text: "This is a statement about Well-Being.",
    },
    {
      keys: { n: '0', o: '0' },
      value: 1,
      name: "NO",
      icon: "",
      text: "This is a statement about Defense.",
    },
    {
      keys: { n: '0', a: '0' },
      value: 1,
      name: "NA",
      icon: "",
      text: "This is a statement about Anger Control.",
    },
    {
      keys: { n: '0', c: '0' },
      value: 1,
      name: "NC",
      icon: "",
      text: "This is a statement about Impulse Control.",
    },
    {
      keys: { e: '0', o: '0' },
      value: 1,
      name: "EO",
      icon: "",
      text: "This is a statement about Interests.",
    },
    {
      keys: { e: '0', a: '0' },
      value: 1,
      name: "EA",
      icon: "",
      text: "This is a statement about Interactions.",
    },
    {
      keys: { e: '0', c: '0' },
      value: 1,
      name: "EC",
      icon: "",
      text: "This is a statement about Activity.",
    },
    {
      keys: { o: '0', a: '0' },
      value: 1,
      name: "OA",
      icon: "",
      text: "This is a statement about Attitudes.",
    },
    {
      keys: { o: '0', c: '0' },
      value: 1,
      name: "OC",
      icon: "",
      text: "This is a statement about Learning.",
    },
    {
      keys: { a: '0', c: '0' },
      value: 1,
      name: "AC",
      icon: "",
      text: "This is a statement about Character.",
    },
  ],
});

data.push({
  version: 0,
  applicant: {
    name: "Kelly Kapoor",
    email: "kelly@example.com",
    phone: "(415) 555-0001",
  },
  org: {
    name: "Acme, Co.",
  },
  job: {
    name: "Sales Associate",
  },
  code: "XYZ123",
  scores: {
    // Bandwidth
    bandwidth: 42,
    intellect: 42,
    focus: 70,
    // Hustle
    hustle: 49,
    hardwork: 49,
    autonomy: 90,
    // Social
    social: 85,
    interpersonal: 85,
    leadership: 10,
  },
  team: {
    id: 0,
    name: "Team Justice",
    icon: "https://storage.googleapis.com/scoutible/www/images/team-icons/TeamIcon_Justice.png",
    text: "People on Team Justice are connectors; they’re the glue that helps keep the project running smoothly. They facilitate communication when needed, but they don’t try to accomplish everything themselves. They’re aware of their limitations and are happy to let others shine where their expertise is greater.",
  },
  statements: [
    {
      keys: { n: '+', o: '+' },
      value: 1,
      name: "Hypersensitive",
      icon: "",
      text: "Seem undefended. Alert to danger and vividly imagine possible misfortunes. Prone to nightmares. Because they think in unusual and creative ways, they may sometimes be troubled by odd and eccentric ideas.",
    },
    {
      keys: { n: '+', c: '-' },
      value: 1,
      name: "Easily Excited",
      icon: "",
      text: "Often at the mercy of their own impulses. Find it difficult and distressing to resist urges or desire, and lack the self-control to hold their urges in check. As a result, they may act in ways that they know are not in their long-term best interests. They may be particularly susceptible to subtance abuse and other health-risk behaviors.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Perfectionist",
      icon: "",
      text: "Will not allow themselves to fail. Great in an enviroment with clear, attainable goals and the opportunity to acheive great things. Can be prone to distress if goals are unattainable or lacking enough structure.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Directed & Driven",
      icon: "",
      text: "Clear sense of own goals and ability to work towards them even under adverse circumstances. Exhibits self-control in chaotic situations requiring self-control and self-imposed realistic expectations.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Fun-Loving",
      icon: "",
      text: "Full of energy and vitality, but they find it hard to channel their energy in constructive directions. Prefer to enjoy life with thrills, adventures, and raucous parties. Prone to be spontaneouns and impulsive, ready to drop work for a good time.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Social Glue",
      icon: "",
      text: "Reach out freely to make new friends. Social glue on a team, helps welcome newcomers to a team. Especially good in a rapidly evolving workplace or a job that involves engaging with many people.",
    },
  ],
});

data.push({
  version: 0,
  applicant: {
    name: "Dwight Schrute",
    email: "dwight@example.com",
    phone: "(415) 555-0002",
  },
  org: {
    name: "Acme, Co.",
  },
  job: {
    name: "Sales Associate",
  },
  code: "XYZ123",
  scores: {
    // Bandwidth
    bandwidth: 34,
    intellect: 34,
    focus: 10,
    // Hustle
    hustle: 92,
    hardwork: 92,
    autonomy: 90,
    // Social
    social: 61,
    interpersonal: 61,
    leadership: 50,
  },
  team: {
    id: 0,
    name: "Team Tycoon",
    icon: "https://storage.googleapis.com/scoutible/www/images/team-icons/TeamIcon_Tycoon.png",
    text: "Pushing hard for success and finding the right ways to motivate people is where people on Team Tycoon excel. They are less focused on the details of how to get things done than in ensuring that things, do indeed, get done as they hustle, constantly pushing the boundaries.",
  },
  statements: [
    {
      keys: { n: '+', o: '+' },
      value: 1,
      name: "Unflappable",
      icon: "",
      text: "Stoic and unconcerned in the face of stressful situations. May not be inspiring, but a solid person to have on your side during a team's ups and downs.",
    },
    {
      keys: { n: '+', c: '-' },
      value: 1,
      name: "Needs Help Organizing",
      icon: "",
      text: "Academic and intellectual pursuits are not the strength or preference for these individuals. They need special incentives to start learning and to stick with it. They may need help in organizing their work and reminders to keep them on schedule. They may have problems maintaining attention.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Go-Getter",
      icon: "",
      text: "Thrives in any environment; self-motivated; works with a rapid tempo. Thrives in situations that require self-direction and a bit of independence.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Prefers Structure",
      icon: "",
      text: "Thrives in a stable environment. Uncomfortable with rapid change.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Thrives on Competition",
      icon: "",
      text: "Tend to view others as potential enemies. They are wary and distant and keep to themselves. Prefer respect to friendship and guard their privacy jealously. When interacting with them, it is wise to allow them the space they feel they need.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Generous",
      icon: "",
      text: "They are giving, sympathetic, and genuinely concerned about others. However, their lack of organization and persistence means they they sometimes fail to follow through on their good intentions. They may be best at inspiring kindness and generosity in others.",
    },
  ],
});

data.push({
  version: 0,
  applicant: {
    name: "Stanley Hudson",
    email: "stanley@example.com",
    phone: "(415) 555-0003",
  },
  org: {
    name: "Acme, Co.",
  },
  job: {
    name: "Sales Associate",
  },
  code: "XYZ123",
  scores: {
    // Bandwidth
    bandwidth: 48,
    intellect: 48,
    focus: 70,
    // Hustle
    hustle: 35,
    hardwork: 35,
    autonomy: 90,
    // Social
    social: 36,
    interpersonal: 36,
    leadership: 10,
  },
  team: {
    id: 0,
    name: "Team Wolfpack",
    icon: "https://storage.googleapis.com/scoutible/www/images/team-icons/TeamIcon_Wolfpack.png",
    text: "Those on Team Wolfpack are loners who tend to themselves, viewing social interaction as a luxury and not a necessity. Living life on their own terms is more important to them than trying to keep up with others, even if they are working harder.",
  },
  statements: [
    {
      keys: { n: '+', o: '+' },
      value: 1,
      name: "Adaptive",
      icon: "",
      text: "Uses negative events as a souce of inspiration. Keenly aware of conflict, stress, and threat, but use these situations to stimulate creative adaptations. Good for flexible thinking within a team in the face of difficulties.",
    },
    {
      keys: { n: '+', c: '-' },
      value: 1,
      name: "Love of Learning",
      icon: "",
      text: "Although not necessarily more intelligent than others, they combine a real love of learning with the diligence and organization to excel. They have a high aspiration level and are often creative in their approach to solving problems. They are likely to go as far academically as their gifts allow.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Modest Yet Responsive",
      icon: "",
      text: "Prefers to be left alone, but are also sympathetic and respond to others' needs. Because they are trusting, others may sometimes take advantage of them.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Calm & Relaxed",
      icon: "",
      text: "See little need to exert rigorous control over their behavior. Tend to take the easy way and are philosophical about disappointments. May need extra assistance in motivating themselves to follow appropriate medical advice or undertake difficult tasks.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Leader",
      icon: "",
      text: "Enjoy social situations and know how to get people to work together. Prefer giving orders rather than taking them. Thrives in an environment where they can have independence, particularly where they can help lead others.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Values Privacy",
      icon: "",
      text: "The interest of these individuals are focused on ideas and activities they can pursue alone. Reading, writing, or creative hobbies (eg painting, music) appeal to them. They prefer occupations that provide both challenge and privacy.",
    },
  ],
});

data.push({
  version: 0,
  applicant: {
    name: "Jim Halpert",
    email: "jim@example.com",
    phone: "(415) 555-0004",
  },
  org: {
    name: "Acme, Co.",
  },
  job: {
    name: "Sales Associate",
  },
  code: "XYZ123",
  scores: {
    // Bandwidth
    bandwidth: 92,
    intellect: 92,
    focus: 70,
    // Hustle
    hustle: 21,
    hardwork: 21,
    autonomy: 30,
    // Social
    social: 83,
    interpersonal: 83,
    leadership: 70,
  },
  team: {
    id: 0,
    name: "Team Savant",
    icon: "https://storage.googleapis.com/scoutible/www/images/team-icons/TeamIcon_Savant.png",
    text: "People on Team Savant like to engage and influence people, but only on their terms. They have the intelligence to persuade people to their line of thinking, but they’re not inspired by every project or every cause that crosses their path.",
  },
  statements: [
    {
      keys: { n: '+', o: '+' },
      value: 1,
      name: "Adaptive",
      icon: "",
      text: "Uses negative events as a souce of inspiration. Keenly aware of conflict, stress, and threat, but use these situations to stimulate creative adaptations. Good for flexible thinking within a team in the face of difficulties.",
    },
    {
      keys: { n: '+', c: '-' },
      value: 1,
      name: "Free-Thinker",
      icon: "",
      text: "Critical thinkers, often unconventional. Good in an environment that values creativity and new ideas.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Optimist",
      icon: "",
      text: "Remain steady even when faced with obstacles; can quickly overcome stress or frustrations. Concentrate on an optimisitc future. Good for helping a team remain positive even in stressful situations.",
    },
    {
      keys: { n: '+', c: '+' },
      value: 1,
      name: "Methodical",
      icon: "",
      text: "Methodical workers who concentrate on the task at hand and work slowly and steadily until it's completed. They can't be hurried but can be counted upon to finish their assigned tasks. Thrives in steady enviroments with clear goals they can work towards. Requires little oversight beyond initial goal setting.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Values Creative Interaction",
      icon: "",
      text: "The interests of these individuals revolve around the new and different, and they like ot share their discoveries with others. They enjoy public speaking and teaching, and they fit in well in discussion groups. They enjoy meeting people from different backgrounds.",
    },
    {
      keys: { n: '-', e: '-' },
      value: 1,
      name: "Social Glue",
      icon: "",
      text: "Reach out freely to make new friends. Social glue on a team, helps welcome newcomers to a team. Especially good in a rapidly evolving workplace or a job that involves engaging with many people.",
    },
  ],
});
