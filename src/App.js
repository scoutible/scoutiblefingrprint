import React, { Component } from 'react';
import each from 'lodash/each';
import 'whatwg-fetch';

import Fingerprint from './components/Fingerprint.js';
import examples from './data/examples.js';

require('./styles/main.scss');

const baseURL = 'https://api.scoutible.com/api/v1';

class App extends Component {

  state = {
    user: {
      name: "User",
    },
    data: {
      version: 0,
      applicant: {
          name: "",
          email: "",
          phone: "",
      },
      org: {
          name: "",
      },
      job: {
          name: "",
      },
      scores: {
          autonomy: 50,
          bandwidth: 50,
          interpersonal: 50,
          focus: 50,
          hardwork: 50,
          hustle: 50,
          intellect: 50,
          leadership: 50,
          social: 50,
      },
      team: {
        name: "",
        icon: "",
        text: "",
      },
      statements: [],
    },
  }

  componentDidMount() {
    const { authToken } = this.props;
    let { reportID } = this.props;
    let fake = false;
    if (!!reportID) {
      const match = reportID.match(/^ex-(\d+)/);
      if (!!match) {
        reportID = match[1];
        fake = true;
      }
    }
    if (authToken) {
      fetch(`${baseURL}/users/~`, {
        headers: {
          Authorization: `Bearer ${authToken}`,
        },
      })
      .then((res) => {
        if (res.ok) {
          return res.json();
        }

        const error = new Error(res.statusText);
        error.response = res;
        throw error;
      })
      .then((data) => {
        this.setState({ user: data });
      })
      .catch((err) => {
        console.error('fetch: error', err);
      });
    }
    if (fake) {
      setTimeout(() => {
        this.setState({ data: examples[reportID] });
      }, 10);
      return;
    }
    if (!reportID) {
      return;
    }
    fetch(`${baseURL}/orgs/applications/report/${reportID}`, {
      headers: {
        Authorization: `Bearer ${authToken}`,
      },
    })
    .then((res) => {
      if (res.ok) {
        return res.json();
      }

      const error = new Error(res.statusText);
      error.response = res;
      throw error;
    })
    .then((data) => {
      console.log('fetch: data', Object.assign({}, data));
      data.job.name = data.job.title;
      delete data.job.title;
      const scores = {};
      each(data.scores, (val, key) => {
        scores[key] = Math.floor(val * 50 + 50);
      });
      data.scores = scores;
      if (!data.statements) data.statements = [];
      this.setState({ data });
    })
    .catch((err) => {
      console.error('fetch: error', err);
    });
  }

  render() {
    return (
      <div className="App">
        <Fingerprint user={this.state.user} fp={ this.state.data } />
      </div>
    );
  }
}

export default App;
